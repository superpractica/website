# Super Practica Website

## Overview

This is the website for Super Practica. Visit it here: <https://superpractica.org/>

This website is built with [Hugo](https://gohugo.io/) using the [Paige theme](https://github.com/willfaught/paige). It is hosted by [Codeberg Pages](https://codeberg.page/). See the ["pages" branch](https://codeberg.org/superpractica/website/src/branch/pages) for the actually hosted site.


## Building

Use Hugo and Paige to build the website. Follow their instructions.

The game is packaged separately. Change links in `content/play/online/_index.md` and `content/play/_index.md` to make it point to a different game.


## Copyright

Copyright 2023-present Super Practica contributors

See <https://codeberg.org/superpractica/superpractica/src/branch/main/AUTHORS.md> for the list of Super Practica contributors.

Website text and images are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](LICENSES/CC-BY-SA-4.0), except where stated otherwise.

Website source code is licensed under the [MIT License](LICENSES/MIT).

Individual source code files might be licensed under different licenses and have different copyright holders. The copyright notice and "SPDX-License-Identifier" near the top of the file will give different copyright information in these cases.

See <https://codeberg.org/superpractica/superpractica/src/branch/main/COPYRIGHT.md> for project-wide copyright information.
