---
title: "Welcome"
description: "Introduction for newcomers."
date: 2023-10-26
lastmod: 2024-10-18
authors: ["svetogam"]
paige:
  style: |
    #paige-collections,
    #paige-sections,
    #paige-title,
    #paige-description,
    #paige-date,
    #paige-authors,
    #paige-toc {
        display: none;
    }
---

## What is Super Practica?
{.center}

Super Practica is a revolutionary game designed to optimize building expertise in mathematics. It is free and open-source software built with [Godot](https://godotengine.org/).

Just by playing it, you become skilled in the topic of the game. It is not frustrating and anyone can play it through to the end. Playing it is far more efficient than any other method of learning that topic. This is not only possible, but is the optimal method for learning mathematics.

---

## Sounds great! How can I play it?
{.center}

I have good news and bad news.

The good news is you're here early and won't miss the start of the show. The bad news is I only have [this lousy demo](/play). Super Practica is still in an early stage of development and isn't fully functional.

{{< paige/image
    alt="Super Practica 0.7.2 screenshot: Grid counting level 3.4, showing a verification of the answer 16 for the problem of 10 + 6 being completed. The count of 6 units on the grid is seen to be identical with the required number 6 in the problem."
    maxwidth="75%"
    options="600x webp picture Lanczos"
    src="screenshots/v0-7-2/v0-7-2-grid-counting-3-4-success.png"
>}}
{.center}

---

## How does it work?
{.center}

The idea is to first make a practice easy and approachable for all beginning players. Then players proceed through a progression of levels that gradually turn them into experts. They finally come to a level that's effectively a simulation of performing the practice just as it would be performed outside the game, so that completing the game logically implies the ability to perform the practice as an expert would. Each step will be empirically verified to ensure that the game works optimally as promised.

This is much easier said than done.

The [Blueprint](/resources/blueprint) is an outline of the theoretical and design work that makes this possible. If you're suspicious of the strong claims I make for Super Practica (as is rational), then I encourage you to read the theory behind it and judge for yourself. I tried to explain it briefly but couldn't do it in less than 100-or-so pages. (100 pages! That's how you know it's legit!)

---

## Who's making this?
{.center}

Actually I was kind of hoping you would. That is, I want this to be a collaborative project and I'm looking for contributions.

As of the latest release it's just me, Svetogam. I am uniquely qualified to develop Super Practica because I have a computer, an internet connection, and the precisely correct skillset to develop the optimal method of reproducing mathematical knowledge.

But making Super Practica will take a lot of work and I couldn't possibly do it alone. So if you're interested, I would appreciate any contribution you can make, whether it's code, money, spreading the word, or something else. See the [Contribute page](/contribute) for how you can help.

---

## How do I know my contributions won't be wasted?
{.center}

There's much that we could salvage from Super Practica if it somehow fails to live up to my claims:

* For some definitions of "failure-proof", [Super Practica is failure-proof.](/resources/blueprint#e.1)
* The code is free software, licensed under the [AGPL](https://www.gnu.org/licenses/agpl-3.0.html), so it can be reused in making other games.
* Assets will be licensed as [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/), so they can also be reused.
* Even if we lose Super Practica, we won't lose the friends we make along the way. That is, the same community and organization will be valuable for making other free and open-source games in Godot.

But I'm pretty sure Super Practica will work as expected. I don't make my claims lightly and I intend to see it through to completion.

If you're interested in contributing but have any doubts or other concerns, then I can answer your questions in the [chat](https://superpractica.zulipchat.com/#narrow/stream/444566-general/topic/Ask.20me.20anything).

---

## Why should I fund Super Practica when I can buy inferior educational games instead?
{.center}

I'm glad you asked! (First of all, [Super Practica is not an educational game.](/resources/blueprint#9.9))

* Educational games rarely function to reproduce practical knowledge and are very inefficient in the rare cases they actually work. (There are some exceptions like for touch typing, but not for mathematics.)
* Super Practica is designed according to the [optimal method](/resources/blueprint#6.8) for reliably and efficiently reproducing practical knowledge. That means educational games will only be effective insofar as their design is similar to Super Practica's design.
* Super Practica is free and open-source, which gives it [many benefits](/resources/blueprint#9.3) over [proprietary games](/resources/blueprint#b.8).
* Being a funder will give you a say over [which topics will have greater priority to be covered](https://codeberg.org/superpractica/wiki/wiki/Funding#individual-benefits).

So funding Super Practica will mean spending *less* money to play a *better* game. You can do this through [Liberapay](https://liberapay.com/SuperPractica/) or [Patreon](https://www.patreon.com/superpractica).

---

## What can I expect to see in the future?
{.center}

We will start with arithmetic and move on from there to other topics of mathematics, and then perhaps beyond mathematics. Arithmetic includes both the basic arithmetic taught in schools and the higher arithmetic known as number theory. The focus will stay on arithmetic until the promise of reliably and efficiently reproducing practical knowledge is realized.

The next thing to look for is a short demo that begins to approximate the plan of the Blueprint. You can see the [Roadmap](/resources/roadmap) for more information and to check up on how that's going.

Keep an eye on the [News](/news) page for updates!
