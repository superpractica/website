---
title: "Contribute"
description: "A guided tour of the organization and of how to contribute"
date: 2025-02-11
authors: ["svetogam"]
paige:
  style: |
    #paige-authors,
    #paige-date,
    #paige-title,
    #paige-toc {
        display: none;
    }
---

## Funding Options

* [Liberapay](https://liberapay.com/SuperPractica/)
* [Patreon](https://www.patreon.com/superpractica)

Funds are used to pay developers and will help very much to improve this project.


## Discussion

[Zulip](https://superpractica.zulipchat.com/) is used for player, funder, and developer discussion.

You can look around the development circles such as [dev-gamedev](https://superpractica.zulipchat.com/#narrow/channel/483189) to find some threads that streamline contribution. For other kinds of contributions, please introduce yourself along with your skills or what you're interested in working on in [new-members](https://superpractica.zulipchat.com/#narrow/channel/445055).


## Repositories

[Codeberg](https://codeberg.org/superpractica) stores the source code and other repositories. You can contribute through it by filing issues or using [git](https://git-scm.com/).

Some major repositories:

* [The game](https://codeberg.org/superpractica/superpractica)
* [The book](https://codeberg.org/superpractica/blueprint)
* [This website](https://codeberg.org/superpractica/website)


## Documentation

The most important documents for the public can be found on the [Resources page](https://superpractica.org/resources/).

You can also find other major documents on the [Wiki](https://codeberg.org/superpractica/wiki/wiki):

* [Theory](https://codeberg.org/superpractica/wiki/wiki/Theory)
* [Design](https://codeberg.org/superpractica/wiki/wiki/Design)
* [Organization](https://codeberg.org/superpractica/wiki/wiki/Organization)


## Development Circles

Super Practica is organized into circles, each of which have their own repositories, chat channels, policies, and various other documents. Contributing will make you a member of one or more of these circles. The [governance repo](https://codeberg.org/superpractica/governance) includes a [list of these circles](https://codeberg.org/superpractica/governance/src/branch/main/circles.md).


## Contact

For private contact, email [contact@superpractica.org](mailto:contact@superpractica.org)
