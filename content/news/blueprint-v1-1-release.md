---
title: "Blueprint v1.1 Released"
description: "Including a synopsis and PDF version"
date: 2025-01-18
authors: ["svetogam"]
---

The [Blueprint](/resources/blueprint/) has been updated to v1.1! It is now also available as a PDF.

[Download PDF](https://codeberg.org/superpractica/blueprint/releases/download/v1.1/blueprint.pdf)

The most notable difference is the addition of a [synopsis](/resources/blueprint/#synopsis) which briefly summarizes the main points of each chapter. And now that I have a better idea of what rather belongs on this website and in other documentation, I found a few more bits I could cut to truly say it's "as brief as I could put it".

See the [Releases page](https://codeberg.org/superpractica/blueprint/releases/tag/v1.1) for the full changelog.
