---
title: "Super Practica v0.6 Demo and Blueprint"
description: "First public release"
date: 2023-10-26
authors: ["svetogam"]
---

This is the first public release of Super Practica! This release consists of a proof-of-concept demo and the "blueprint", which is kind of like a design document. The demo doesn't tell the whole story, so it's necessary to read the blueprint to tell what Super Practica is really about.

[Super Practica v0.6 Demo](/play/)

[Blueprint](/resources/blueprint/)

[Video playthrough of the demo](https://www.youtube.com/watch?v=3lKQDs11VVg)

The demo is a proof of concept in terms of user interface, game architecture, and progression within and between levels. I call it "v0.6" to reflect the many prototypes and changes of direction I have made up to this point. It's kind of hard to tell, but it contains the seeds of a revolution. I have explained the idea as briefly as I could in the blueprint. You can also find an explanation of the demo in the description of the demo video.

I hope that seeing the demo and blueprint will persuade some developers and prospective players to contribute to this project. If you're interested, then please see the [Contribute page](/contribute/)! Super Practica is too big for me to make alone, and I will need funding to work on it with full focus and financial security. At this early stage, even small contributions will be very helpful and very appreciated!

