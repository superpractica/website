---
title: "Super Practica v0.7.1 Released"
description: "Speed Status: Stalwart Snail"
date: 2024-09-30
authors: ["svetogam"]
---

You can now play [Super Practica v0.7.1](/play)! This release contains a new level set featuring an improved verification system, back after being cut in the [last release](/news/v0-7-release).

According to the theory of the [Blueprint](/resources/blueprint), verification is [the glue that holds mathematics together](/resources/blueprint#1.10):

> Most mathematical practices are built on top of other mathematical practices, so we can verify the higher practice by the lower. But some mathematical practices are self-sufficient. They include counting, recognizing symmetry in a picture, and noticing the identicality of signs.—These are the foundations of the verificational structure of mathematics.

This new level set features verification by counting and by noticing the identicality of signs, so these necessary types of verification can be crossed off the list. The rest will follow. And when they are all done?—Super Practica will be a *general solution* to the problem of [mechanizing](/resources/blueprint#2.1) the verificational structure of the [game of mathematics](/resources/blueprint#1.1).

But it took 2 months to get just 1 level set! Could Super Practica ever get to the point of covering all of mathematics? I think so. Mind the acceleration: 0.5 level sets per month is a great increase from 0.11 level sets per month for the last release. This marks the graduation of Super Practica's speed status from *Resolute Rock* to *Stalwart Snail*!

And the speed of progress will only continue to increase from here. This release took longer than it could have because I have spent much time on improving the architecture and automating the release process. It reflects my greater concern to not repeat the mistakes of so many ambitious games that push out content quickly and then atrophy and collapse under their own weight. My first priority is for the [tree of Super Practica](/resources/blueprint#8.11) to grow strong roots that will enable its future growth. This means slow, but steady—*and accelerating*—progress.

If you want to help accelerate its progress a little more, check out the [Contribute](/contribute) page.
