---
title: "Super Practica v0.7.2 Released"
description: "Fun Factor: Exists"
date: 2024-12-23
authors: ["svetogam"]
---

Super Practica v0.7.2 is now playable on the [Play](/play) page!

It contains a (mostly) completed topic on "Grid-Counting", approximating what you can expect to see in a complete release. As you play, you might be surprised to find it's just a little bit *fun*.

This might be strange because [fun is not a priority](/resources/blueprint/#9.13) in Super Practica's design. Many games are designed to be fun, even to maximize fun, but the aim of Super Practica is only to efficiently reproduce practical knowledge. So why is it just a little bit fun now, and why will it be more fun later?---Because *mathematics is inherently fun*. By removing frustrations, like removing so much dust or brush, the natural fun of mathematics will shine through more clearly and brightly.

This demo also illustrates a part of the "microscopic" level-progression of Super Practica (specifically points (2), (3), and (4) from [Blueprint.7.4](/resources/blueprint/#7.4) and [Blueprint.7.5](/resources/blueprint/#7.5).) The effect is that the Grid-Counting topic can be picked up and played by anyone who can count and use a computer, and that they can play through the levels without getting stuck to reach the end.

What's important here is that the design and game framework of Super Practica make this a *general solution*. So what you see is only an example of a progression that can be repeated over and over to introduce more and more mathematics. This solution is, however, not yet complete in full generality. More core mechanics will be necessary to [cover](/resources/blueprint/#8.3) all of arithmetic.
