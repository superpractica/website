---
title: "Super Practica v0.7 Demo Released"
description: "First steps on the road to a player-facing demo."
date: 2024-07-29
authors: ["svetogam"]
---

Super Practica v0.7 is now playable on the [Play](/play) page!

The highlights of this release are the redesigned primary interface and level-selection screen. These are finally decent (not thrown together with wild abandon for prototyping purposes) and now approximate what they will look like in the player-facing demo.

On the content side, the levels of the previous demo have been cut, and only one small set of simple levels replaces them. This demo doesn't function as a proof of concept anymore, but these levels represent the first steps on the path to eventually, hopefully, [covering the whole rest of mathematics](/resources/blueprint/#8.11) with [the optimal method of reproducing practical knowledge](/resources/blueprint/#6.8). You couldn't tell just by looking at them though.

A lot of work has been done on the backend and infrastructure side, which is what I have focused on for the 9 months since the last release. Now with this work out of the way, such as porting Super Practica to Godot 4, Super Practica is ready for a lot more content to be added. Stay tuned for more content in coming releases!

Looking at the new infrastructure since the first release, the highlights are the [wiki](https://codeberg.org/superpractica/wiki/wiki/Home) which is beginning to get filled out with documentation, and the [Zulip chat](https://superpractica.zulipchat.com), which will hopefully serve as a good home for discussion about Super Practica going forward.

Now, you might be wondering what a zulip is and why you should chat with it. In short, [Zulip](https://zulip.com/) distinguishes itself from other chat software by being better suited to productive projects and by kindly sponsoring Super Practica with free hosting. While it's a little unusual as a chat program, I think it's a good fit. So if you're interested in Super Practica and want to help or ask questions, consider [stopping by](https://superpractica.zulipchat.com/register)! Don't forget to check the [Contribute](/contribute) page along your way.
