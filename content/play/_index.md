---
title: "Play"
description: "Ways to play Super Practica."
date: 2023-10-26
lastmod: 2024-09-30
paige:
  style: |
    #paige-toc,
    #paige-title,
    #paige-description,
    #paige-date,
    #paige-sections {
        display: none;
    }
---

The latest release is Super Practica {{< version >}}.

This is only an early demo release. No promises are made regarding its effectiveness or playability. Major changes and additions are still planned.

For best experience, download the game and play on a desktop computer.

---

[**Play Online**](online)
{.center, .larger}

[Watch Gameplay Video](https://www.youtube.com/watch?v=MLfphJ1AaRc)
{.center, .larger}

[View Changelog](https://codeberg.org/superpractica/superpractica/src/branch/main/CHANGELOG.md)
{.center, .larger}

---


## Download

[Linux (64-bit)](https://codeberg.org/superpractica/superpractica/releases/download/v0.7.2/SuperPractica-linux64.zip)

[Linux (32-bit)](https://codeberg.org/superpractica/superpractica/releases/download/v0.7.2/SuperPractica-linux32.zip)


[Windows (64-bit)](https://codeberg.org/superpractica/superpractica/releases/download/v0.7.2/SuperPractica-win64.zip)

[Windows (32-bit)](https://codeberg.org/superpractica/superpractica/releases/download/v0.7.2/SuperPractica-win32.zip)


## Screenshots

{{< paige/figure
>}}
{{< paige/image
    alt="Super Practica 0.7.2 screenshot: Level-select screen."
    maxwidth="100%"
    options="600x webp picture Lanczos"
    src="screenshots/v0-7-2/v0-7-2-grid-counting-levels.png"
>}}
{{< paige/image
    alt="Super Practica 0.7.2 screenshot: Grid counting level 2-1."
    maxwidth="100%"
    options="600x webp picture Lanczos"
    src="screenshots/v0-7-2/v0-7-2-grid-counting-2-1-success.png"
>}}
{{< /paige/figure >}}

{{< paige/figure
>}}
{{< paige/image
    alt="Super Practica 0.7.2 screenshot: Grid counting level 3-3."
    maxwidth="100%"
    options="600x webp picture Lanczos"
    src="screenshots/v0-7-2/v0-7-2-grid-counting-3-3-failure.png"
>}}
{{< paige/image
    alt="Super Practica 0.7.2 screenshot: Grid counting level 4-2."
    maxwidth="100%"
    options="600x webp picture Lanczos"
    src="screenshots/v0-7-2/v0-7-2-grid-counting-4-2-progress.png"
>}}
{{< /paige/figure >}}
