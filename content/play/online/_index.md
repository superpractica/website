---
title: "Play Online"
description: "Super Practica {{< version >}}"
date: 2023-10-26
lastmod: 2024-09-30
type: play-online
game:
  # This source is obtained from https://svetogam.itch.io/super-practica
  source: "https://html-classic.itch.zone/html/11520241-1148398/index.html"
  title: "Super Practica Demo"
  width: 800
  height: 600
paige:
  style: |
    #paige-title,
    #paige-date{
        display: none;
    }
---
