---
title: "Roadmap"
description: "Overview of planning and progress for future releases"
date: 2024-12-23
authors: ["svetogam"]
---

## Current Release: Pre-Player-Facing Demo 3 (v0.7.2)

For summaries of current and past releases, see the [Changelog](https://codeberg.org/superpractica/superpractica/src/branch/main/CHANGELOG.md).


## Next Major Release: Player-Facing Demo (v0.8.0)

### Overview

Releasing a player-facing demo is the top priority. This demo will initially have only 1 [pim](/resources/blueprint/#2.7) and 1 topic containing levels progressing from 1-digit to 2-digit addition. The priority will be to make the start of the full progression of the game enjoyable and easily playable.

While low on content, the point is to develop functioning core features that can be reused in future content. Focus will also be given to developing infrastructure and backend systems that will enable faster and more effective development in the future.

This release will also make Super Practica presentable by adding simple graphics, but it will mostly have only stock graphics at this point.

[View issues](https://codeberg.org/superpractica/superpractica/issues?type=all&state=open&milestone=4782)

### Features

* [x] [Multiscopic level-selection map](/resources/blueprint#7.2)
* [x] [Pimnet](/resources/blueprint#2.5) interface redesign
* [ ] (3/4) Goal-mechanics enabling 4 different types of levels

### Content

* [x] Grid-Counting pim
* [ ] (27/29) Grid-Counting levels

### Architecture and Backend

* [x] Upgrade to Godot 4
* [x] Make the interface backend less convoluted
* [x] Improve the core layer for quickly making extensible content

### Aesthetics

* [ ] Simple presentable graphics

### Infrastructure

* [x] Make a .pdf version of the [Blueprint](/resources/blueprint) downloadable on the website


## Following Major Release: v0.9.0?

Following the player-facing demo, the greatest priority will be to begin to set up a sustainable and effective organization for future development. This will include making explanation videos, streamlining the contribution process, documenting the reasoning for design decisions to enable [logical collaborative design](/resources/blueprint#9.5), and a lot more.

Next to these things, new content in the form of levels and pims will continue to be released at a gradual pace. The result will be a continuously expanding tree of levels covering more and more mathematics. It should be possible to keep this up in combination with the other things because much effort has already went into making effective backend code and core features.

It will be important that some of the new content will be [topics from outside common school curricula](/resources/blueprint/#9.4), so that most adults will be able to learn something new by playing Super Practica. Together with this, some system will be set up to let funders vote on which topics will be covered sooner.

### Features

* [ ] (0/3?) More goal-mechanics to enable more types of levels

### Content

* [ ] Board-Grouping pim
* [ ] (0/19) Board-Grouping levels
* [ ] Grid-Tracking pim
* [ ] (0/48) Grid-Tracking levels

### Design

* [ ] (0/8?) Design and publish design-specs for much content

### Infrastructure

* [ ] Document workflow for creating content
* [ ] Lots of [design documentation](https://codeberg.org/superpractica/wiki/wiki/Design)
* [ ] Much [theory documentation](https://codeberg.org/superpractica/wiki/wiki/Theory)
* [ ] (0/10?) Set up streamlined contribution roles
* [ ] Member voting system

*(Incomplete. More to be determined later.)*


## Later

I'll have to see what interest there is in Super Practica before continuing to plan from here.

Continuing to make the game alone does not appear to be a good plan due to how large its scope is. Its core concept and value should by now be proven, so I should find ways to begin to move away from programming to focus more on management and design.


## Eventual Release: v1.0

Version 1.0 will be something that I could begin to recommend as approaching "the optimal method of learning mathematics". Its content coverage will include a main line of Arithmetic from counting to pre-algebra and some topics of Number Theory.

The progression will advance not only to more advanced Arithmetic, but also to simulations where the Arithmetic is applied.---That is, to [Super Practica B](/resources/blueprint/#4.9).

The priority is to complete a full line of progression through [Super Practica A, B, and C](/resources/blueprint#6.16). This means that it will make mathematics easily playable, it will simulate some problem-domains in which mathematics is used, and it will have a reliable level-progression to make expertise acquirable just by playing the game.

This line of progression being completed will enable the [empirical framework of testing](/resources/blueprint#8.1) and iterative redesign to begin to function. In preparation for this release, some empirical testing should be done to see if Super Practica actually works or if it will need a major redesign.

It will have more than stock graphics. That Super Practica should look nice is very important.


## Distant Future

From here we will be able to develop Super Practica to perfect the [playability](/resources/blueprint/#2.3), [playthruability](/resources/blueprint/#6.5), [efficiency](/resources/blueprint/#6.7), and [essential accuracy](/resources/blueprint/#4.5) of its existing content. We will be able to branch out to cover arithmetic more completely. And we will try to progress to Algebra and Geometry, if this turns out to be viable.
